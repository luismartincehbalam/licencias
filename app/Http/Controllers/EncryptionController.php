<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class EncryptionController extends Controller
{
    // nueva funcion 

    public function encrypt(){
        $encrypt = Crypt::encryptString('esto es un mensaje para todos esta es la mejor clase');
        return $encrypt; 
    }
    public function decrypt(){
        $decrypted = Crypt::decryptString('eyJpdiI6IkU4TE1hcmVmT0RieGNPYmR1a3dVTEE9PSIsInZhbHVlIjoiRDlYclhVQU45QW9yRU14Z2xCMnQzc0JwZ2IybERTdTViNmxzbGlYQlJOUUlkMytidno5ajBhODZSUUxjQ1JhbnNRRG1vVWRaVDhIWnhZeFwvZm4xYXhnPT0iLCJtYWMiOiJhODZlOWVmMjQ4Yzc4Yjc0MDUxNTZhNjZhN2M1NmIxODI0MjlhOWY2MzVmNzZlNmU4YTgzYTcwNzc1YmM3MDhlIn0=');
        return $decrypted;
    }

}
